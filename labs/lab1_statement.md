# Lab 1 statement

The objective of  first lab is to illustrate through simple examples and methods the basic notions of machine learning: under/overfitting trade-off, the  performance evaluation and model selection.

*Note: For each notebook, read the cells and run the code, then follow the instructions/questions in the `Exercise` cells.*

##### I. Nearest Neighbors

  See the notebooks in the [2_knn](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/notebooks/2_knn/) folder:
  1. apply k-NN on a toy 2D data set, [`N1_blobs_knn.ipynb`](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/notebooks/2_knn/N1_blobs_knn.ipynb)
  2. apply and evaluate k-NN for different choices of hyperparameters on the Iris data set, [`N2_iris_knn.ipynb`](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/notebooks/2_knn/N2_iris_knn.ipynb)

##### II. Model Assessment

  See the notebooks in the [3_model_assessment](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/notebooks/3_model_assesment/) folder:

  1. apply cross-validation to tune (i.e. to select the best) hyperparameters on different datasets, [`N1_validation_and_model_selection.ipynb`](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/notebooks/3_model_assesment/N1_validation_and_model_selection.ipynb),
  2. Get a proper (i.e.~ *unbiased*) estimate of the true error while tuning the hyperparameters [`N2_nested_cross_validation_iris.ipynb`](https://gricad-gitlab.univ-grenoble-alpes.fr/chatelaf/ml-sicom3a/-/blob/master/notebooks/3_model_assesment/N2_nested_cross_validation_iris.ipynb)
